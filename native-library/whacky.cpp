#include "whacky.h"
#include <iostream>
#include <string>
#include <vector>
#include <functional>
#include <locale>
#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <codecvt>

#elif __linux__
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#endif

const int max_len = 298;

#define min(a,b) ( (a) < (b) ? (a) : (b))

#ifdef _WIN32

WINBOOL CALLBACK enum_windows_callback(HWND hWnd, LPARAM lParam)
{
    wchar_t* className = new wchar_t[255]{};
    wchar_t* windowName = new wchar_t[255]();
    GetClassNameW(hWnd, className, 255);
    GetWindowTextW(hWnd, windowName, 255);
    //convert wide string (UTF-16) to UTF-8 string
    std::u16string source;
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
    //GetWindowTextW
    printf("%s, %s\n",
            converter.to_bytes(className).c_str(),
            converter.to_bytes(windowName).c_str()
            );
    delete className;
    delete windowName;
    return TRUE;

}

void lookup_windows_win32() {
    //TODO Get HDESK with OpenDesktopA
    //Pass thread to GetThreadDesktop
    DWORD threadId = GetCurrentThreadId();
    HDESK hDesk = GetThreadDesktop(threadId);
    //this calls the callback function
    EnumDesktopWindows(hDesk,
                       enum_windows_callback,
                       0);
}

#elif __linux__

//taken from xprop.c
static const char *
XLib_Get_Window_Property_Data (Display* dpy, Window target_win, Atom atom,
                          long *length, int *size)
{
    Atom actual_type;
    int actual_format;
    unsigned long nitems;
    unsigned long nbytes;
    unsigned long bytes_after;
    static unsigned char *prop = NULL;
    int status;

    if (prop)
    {
        XFree(prop);
        prop = NULL;
    }

    status = XGetWindowProperty(dpy, target_win, atom, 0, (max_len+3)/4,
                                False, AnyPropertyType, &actual_type,
                                &actual_format, &nitems, &bytes_after,
                                &prop);
    if (status == BadWindow)
        return 0;
    if (status != Success)
        return 0;

    if (actual_format == 32)
        nbytes = sizeof(long);
    else if (actual_format == 16)
        nbytes = sizeof(short);
    else if (actual_format == 8)
        nbytes = 1;
    else if (actual_format == 0)
        nbytes = 0;
    else
        return 0;
    *length = min(nitems * nbytes, max_len);
    *size = actual_format;
    return (const char *)prop;
}
void lookup_windows_xlib()
{
    Display* dpy = XOpenDisplay(NULL);
    int screen_number = XDefaultScreen(dpy);
    Window root_window = XRootWindow(dpy, screen_number);
    Window wr, wp;
    Window* wc;
    unsigned int nchildren;
    XQueryTree(
            dpy,
            root_window,
            &wr,
            &wp,
            &wc,
            &nchildren
    );
    long length;
    int size;

    std::string output_text;
    const char* data;
    for (int i = 0; i < nchildren; i++) {
        data = XLib_Get_Window_Property_Data(
                dpy,
                wc[i],
                XA_WM_CLASS,
                &length,
                &size
        );
        if (data) {
            output_text += data;
            output_text += " - ";
            data = XLib_Get_Window_Property_Data(
                    dpy,
                    wc[i],
                    XA_WM_NAME,
                    &length,
                    &size
            );
            if (data) {
                output_text += data;
            }
            output_text += "\n";
        }
        printf("%s",output_text.c_str());
    }
    delete data;
    delete wc;
}
#endif

DLLEXPORT void lookup_windows()
{
#ifdef __linux__
    lookup_windows_xlib();
#elif _WIN32
    lookup_windows_win32();
#endif
}
