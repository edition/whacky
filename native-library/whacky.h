//
// Created by ben on 10/08/19.
//

#ifndef NATIVE_LIBRARY_WHACKY_H
#define NATIVE_LIBRARY_WHACKY_H

#include <string>

#ifdef __cplusplus
extern "C" {
#   if (defined(__GNUC__) && ((__GNUC__ > 4) || (__GNUC__ == 4) && (__GNUC_MINOR__ > 2))) || __has_attribute(visibility)
#   define DLLEXPORT     __attribute__((visibility("default")))
#   define DLLIMPORT     __attribute__((visibility("default")))
#else
#define DLLEXPORT
  #define DLLIMPORT
#   endif
#endif
DLLEXPORT void lookup_windows();
#ifdef __cplusplus
};
#endif
#endif //NATIVE_LIBRARY_WHACKY_H
