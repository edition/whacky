#!/bin/bash
pushd native-library
if [ ! -d build ]; then
	mkdir build
else
	rm -rf build/
	mkdir build
fi
pushd build
cmake ..
cmake --build . --target whacky -- -j2
popd
popd
