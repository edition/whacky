# About
whacky is a wrapper library for windowing API's such as XLib.
Planning to support other systems in the near future.

# Building for Linux
 - Install cmake
 - Install X11 libraries (if not already there)
 - Run `source ./build.sh`

# Building for Win32
 - If you are compiling for Windows from Linux using the MinGW toolchain, when configuring make sure to pass the following to cmake
   `-DWIN32=1 -DCMAKE_C_COMPILER=/usr/bin/i686-w64-mingw32-gcc -DCMAKE_CXX_COMPILER=/usr/bin/i686-w64-mingw32-c++`
 - And then run `cmake -DWIN32=1 --build . --target test.exe -- -j 2`

# Testing
Run `python test/test.py` to test the library.


